function loadCSS(href){
    var ss = window.document.createElement('link'),
    ref = window.document.getElementsByTagName('home')[0];

    ss.rel = 'stylesheet';
    ss.href = href;
    ss.media = 'only x';

    ref.parentNode.insertBefore(ss, ref);

    setTimeout( function(){
      ss.media = 'all';
    },0);
  }
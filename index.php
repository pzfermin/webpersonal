<?php
    require_once("php/includes/head.php");
    require_once("php/includes/header.php");
?>
    <main>
        <!-- ABOUT -->
        <div class="about-me" id="about">
            <div class="row">
                <div class="col-sm-4"></div>
                <h2 class="col-sm-4">Sobre</h2>
                <div class="col-sm-4"></div>
            </div>
            <div class="row">
                <div class="col-sm-4"></div>
                <h3 class="col-sm-4">Mis Trabajos</h3>
                <div class="col-sm-4"></div>
            </div>
            
            <div class="container">
                    <hr>
                <div class="row">
                        <div class="col-sm-2"></div>
                        <p class="col-sm-8">
                            <span class="text-uppercase">PASION POR EL DESARROLLO</span></br>
                            Soy un aparasionado por mi trabajo. Siempre lo hice con gusto y con la motivacion suficiente para superarme todos los días. Hoy me lanzo a nivel profecional y con la responsabilidad que me caracteriza.
                        </p>               
                        <div class="col-sm-2"></div>
                </div>
                <img src="img/webresponsive.png" alt="responsiveweb" class="img-fluid"id="responsiveweb">
            </div>
        </div>
        <!-- SERVICIOS -->
        <div class="my-services" id="proyecto">
                <div class="row">
                    <div class="col-sm-4"></div>
                    <h2 class="col-sm-4" >Mis Servicios</h2>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <h3 class="col-sm-4">Precencia / Webs corporativas / E-Comerce</h3>
                    <div class="col-sm-4"></div>
                </div>
                
                <div class="container">
                        <hr>
                    <div class="row">
                            <div class="col-sm-2"></div>
                            <p class="col-sm-8">
                                <span class="text-uppercase">Cada proyecto es único</span></br>
                                Pongo en cada proyecto todo mi esfuerzo por lograr el mejor resultado hasta en el mas minimo detalle. 
                            </p>               
                            <div class="col-sm-2"></div>
                    </div>
                  <a href="servicios.php"> <img src="img/bombilla.png" alt="responsiveweb" class="img-fluid" id="bombilla"></a>
                </div>
            </div>
            <div class="my-profile" id="profile">
                <div class="row">
                    <div class="col-sm-1 "></div>
                    <h2 class="col-sm-4 sm-parrafos" id="whoiam">Sobre mi</h2>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-1"></div>
                    <h3 class="col-sm-4 sm-parrafos parrafo-about" id="iam">FERMIN PAEZ</h3>
                    <div class="col-sm-4"></div>
                </div>
                
               <!-- <div class="container">-->
                        
                    <div class="row">
                            <div class="col-xs-1 col-sm-1 col-md-1"></div>
                            <p class="col-xs-6 col-sm-6 col-md-6 sm-parrafos" id="parrafo-profile">
                                <span class="text-uppercase">Soy un desarrollador</span></br>
                                En los ultimos años, mientras trabajaba en otros rubros </br> me dedique a estudiar </br> para convertirme en un desarrollador web! 
                            </p>               
                            <div class="col-xs-5 col-sm-5 col-md-6"></div>
                    </div>
                
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <img src="iconos/bootstrap/icons8-oreja-96.png" class="iconos"  id="icono1" alt="">
                        <img src="iconos/css/icons8-css3-96.png" class="iconos" id="icono2" alt="">
                        <img src="iconos/git/icons8-git-96.png" class="iconos" id="icono3" alt="">
                        <img src="iconos/html5/icons8-html-5-96.png" class="iconos" id="icono4" alt="">
                    </div> 
                    <div class="row">   
                        <div class="col-sm-1"></div>    
                        <img src="iconos/js/icons8-js-96.png" class="iconos" id="icono4" alt="">
                        <img src="iconos/json/icons8-json-96.png" class="iconos" id="icono5" alt="">
                        <img src="iconos/mysql/icons8-mysql-80.png" class="iconos" id="icono6" alt="">
                        <img src="iconos/php/icons8-php-100.png" class="iconos" id="icono7" alt="">
                    </div>
                    
                <!--</div>-->
            </div>
            <div class="col-md-12"id="Contacto"></div>
           
            <div class="row" id="rowContacto">
                    <div class="col-xs-2 col-sm-2 col-md-1"></div>
                <div class="col-xs-8 col-sm-8 col-md-3 col-lg-2" id="infoContacto">
                    <h2 class="contacto">Contacto</h2>
                    <hr>
                    <p class="contacto"> Fermin Paez</p>
                    <div class="row"></div>
                    <p class="contacto">Gregorio Jurez 346, Monte hermoso, Buenos Aires </p><hr>
                    <p class="contacto">Azcuenaga 1378, Monte Grande, Buenos Aires</p><hr>
                    <p class="contacto">WhatsApp: 2914610704</p><hr>
                    <p class="contacto">Telefono: 02921482634</p><hr>
                    <p class="contacto">pzfermin@gmail.com </p>
                    <p class="contacto">pzfermin@ba-developers.com </p>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-1"></div>
                <!-- Formulario de contacto-->
                <div class="col-xs-2 col-sm-2 col-md-1 col-lg-0 col-xl-0 "></div>
                <div class="col-md-5 col-sm-8 col-lg-6" id="formulario">
                    <form class="container" action="/php/ingresadatos.php">
                        <div class="form-group">
                            <label for="formGroupExampleInput">Nombre</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Enter your full name">
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">E-mail</label>
                            <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Enter your E-mail">
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Telephone number</label>
                            <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Enter your Telephone number">
                        </div> 
                        <div class="form-group">
                            <label for="comentario">Comentario</label>
                            <textarea class="form-control" name="" id="" cols="30" rows="5" placeholder="Input yor commentary"></textarea>
                        </div>
                    </form>
                </div>    
                </div>
        


    </main>

<?php
    require_once("php/includes/footer.php");
?>

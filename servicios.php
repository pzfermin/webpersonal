<?php
    require_once('php/includes/head.php'); 
    require_once('php/includes/header.php');
?>
<main>
    <div class="container fluid">
        <div class="row">           
            <h1 class="" id="titulo-servicios">Servicios</h1> 
            <br>           
        </div>
    </div>
    <div class="row">
        <!--seccion 1-->
        <div class="container col-sm-4">
            <div class="row col-sm-12">
                <h1>Precencia Web</h1>
            </div>
            <div class="row col-sm-12">
                <p>La precencia web es el punto de partida para toda organizacion. Esta pagina, aunque simple, le ayudara a hacerce conocer.</p>            
            </div>
            <div class="row col-sm-12">
                <img src="img/world-search.png" alt="imagen 1" id="mundo">
            </div>
            <div class="row col-sm-12">
                <a class="col-sm-12 btn btn-outline-warning " id="seleccionar">Seleccionar!</a>
            </div>
        </div>
        <!--seccion 2-->
        <div class="container col-sm-4">
            <div class="row col-sm-12">
                <h2>Tu Web Crece</h2>
            </div>
            <div class="row col-sm-12">
                <p>A medida que la organizacion comienza a crecer es necesario agregar mayor cantidad de paginas dentro de la web.</p>
            </div>
            <div class="row col-sm-12">
                <img src="img/responsive.png" alt="imagen 2" id="responsive">
            </div>
            <div class="row col-sm-12">
                <a class="col-sm-12 btn btn-outline-warning " id="seleccionar">Seleccionar!</a>
            </div>    
        </div>
        <!--seccion 3-->
        <div class="container col-sm-4">
            <div class="row col-sm-12">
                <h2>E-Commerce</h2>
            </div>
            <div class="row col-sm-12">
                <p>Con esta opcion usted podra vender sus productos o servicios al mundo.</p>
            </div>
            <div class="row col-sm-12">
                <img src="img/e-comerce.png" alt="imagen 3" id="chango">
            </div>
            <div class="row col-sm-12">
                <a class="col-sm-12 btn btn-outline-warning " id="seleccionar">Seleccionar!</a>
            </div>
        </div>
    </div>
    <br>
<?php
    require_once("php/includes/footer.php");
?>
